>_Date:_ Lundi 20 mars 2023.
>
> **Mastère Spécialisé - Infrastructure Cloud et  DevOps 2022-23**
> GROUPE 3
>


# Mise en place de l'environnement d'exécution de l'application meteo-icd

# Présentation

Ce dépôt permet de déployer un cluster rancher  kubernetes  avec la chaîne de déploiement continue Gitlab de ce projet. 

Les playbook et les rôles ansible sont utilisés pour installer ce cluster.  

Ainsi,

- [x]  le playbook `environment.yml `installe les dépendances python et les modules necessaire à ansible pour faire les déploiements de paquetage helm ou  de manifests kubernetes; il installe aussi l'outil `kubectl` pour l'utilisateur ansible;
- [x] le rôle  `rancher.k8s` dans le playbook `rke_deploy.yml` installe et configure rancher ;
- [x]  le rôlle `traefik.k8s` dans le playbook `ingress.yml` installe et configure le contrôleur ingress.


Le contrôleur `ingress` est deployé dans le cluster pour assurer la fonction de `LoadBalancer`.

> _**NB**_: l'agent d'observabilité `filebeat` est embarqué dans le controleur ingress avec un sidecar déclaré dans le fichier de valeurs de chart `values.yml.j2`. Il requiert  un `configMap` nommé `traefik-logs` qui est créé grâce au template `filebeat-configmap.yaml.j2` avant le déploiement du contrôleur (cf. playbook `ingress.yml`).

# Utilisation du dépôt

Le fichier de chaîne de déploiement continue `.gitlab-ci.yml` dans ce dépôt construit une image d'exécution de job ansible  à l'aide du Dockerfile à la racine du projet. Il exécute d'abord les jobs d'un premier stag`prepare` pour installer toutes les dépendances de librairies python, les rôles et les collections qui sont indispensables pour les jobs du stage de déploiement `deploy`.

Le déploiement est assuré par le stage `deploy`.  Pour des besoins de mise à jour dans la gestion du cycle de vie du cluster, il est prévu un stage `update`.  Il existe une dépendance entre les deux jobs du stage `deploy`.  Ce qui permet de déployer d'abord le cluster rancher avant le contrôleur ingress.


## Pré-requis

Pour personnaliser le déploiement , il faut disposer dans le projet des fichiers de configuration ansible et SSH avec l'inventaire des noeuds. Normalement tout ceci est rendu transparent grâce à leur téléchargement automaque du registre de paquetage  du projet de déploiement de l'infrastructure Openstack. 

La seule informations nécessaire est la clé privée autorisée sur les instances comptabilisées dans le fichier d'inventaire. Il s'agit des variables Gitlab de type fichier :  la paire `OPENSTACK_PRIV_KEY` et  `OPENSTACK_PUB_KEY`. 

 En cas d'expiration ou de révocation des jetons de déploiement il sera nécessaire de les mettre à jour à travers les variables  `CI_ANSIBLE_TOKEN_USERNAME`, `CI_ANSIBLE_TOKEN` et `CI_GITLAB_REGISTRY_TOKEN` toutes définies au niveau groupe de projets.

Pour les configuration avancée se référer aux fichiers de variables suivants:

-  `group_vars/masters.yml` :  pour définir lpar exemple une nouvelle version de helm, de kubectl et de traefik;
-  `group_vars/k8s.yml`: pour définir la version de  rancher, de kubernetes et les blocs CIDR des réseau de services de cluter et de pod.


## Déploiement du cluster rancher kubernetes

- [x] Enregistrer un runner
 
Le runner doit pouvoir faire du Docker in Docker (DinD) avec les tags iac, master.

```bash
sudo gitlab-runner register --url https://gitlab.imt-atlantique.fr \
       --name docker-executor-for-vapormap \
       --tag-list iac,master --executor docker
```


- [x] Modifier les variables Gitlab liées au nouvel environnement selon les pré-requis, notamment `OPENSTACK_PRIV_KEY`,  et `OPENSTACK_PUB_KEY` et au besoin celle des jetons `CI_ANSIBLE_TOKEN_USERNAME`, `CI_ANSIBLE_TOKEN` et `CI_GITLAB_REGISTRY_TOKEN` en cas d'expiration ou de révocation.

[x] Déclencher manuellement un nouveau cycle d'exécution du pipeline GitLab (CI/CD > Pipelines > Bouton Run pipeline) ou automatiquement par itération avec les commit en fixant un `tag` satifaisant la règle `*-release`. Le dernier `tag` stable à la date de rédaction de ce README étant `1.70-release`.

```sh
git add .
git commit -u origin master
git push -u origin master
git tag -a 1.70-release -m "Update" && git push -u origin 1.70-release
```
- [x] Déclencher manuellement l'exécution des jobs du stage `deploy` pour déployer successivement le cluster `rancher` kubernetes suivi du contrôleur ingress pour une version de release.

- [x] Déclencher manuellement l'exécution des jobs du stage `update` pour mettre à jour le cluster `rancher` ou du contrôleur ingress `traefik`.


## Test
Utiliser la clé SSH autosisée pour se connecter au noeud  `control1` qui est celui par défaut sur lequel l'outil `kubectl` est installé et configurer pour le cluster. Vérifier ensuite que kubernetes est bien installé avec la commande :
```sh
$ kubectl get all --all-namespaces
``` 


